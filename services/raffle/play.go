package raffle

import (
	"math/rand"
	"time"

	"bitbucket.org/art-es/slotegrator_go/database"
	"bitbucket.org/art-es/slotegrator_go/models"
	"github.com/jinzhu/gorm"
)

const (
	// PrizeTypeMoney is a money from player balance, which
	// 	player can get to himself bank account.
	PrizeTypeMoney = "money"

	// PrizeTypePoints is a points, in-game currency.
	PrizeTypePoints = "points"

	// PrizeTypeGift is a something from gift table.
	PrizeTypeGift = "gift"

	maxMoney = 100
	minMoney = 10

	maxPoints = 100
	minPoints = 10
)

// Play raffle & get prize.
func (r *Raffle) Play() {
	r.randomPrize()
}

func getRandomPrizeType() string {
	prizeTypes := []string{
		PrizeTypeMoney,
		PrizeTypePoints,
		PrizeTypeGift,
	}
	randIndex := rand.Intn(len(prizeTypes))

	return prizeTypes[randIndex]
}

func (r *Raffle) randomGift() {
	db, _ := database.Connect()
	defer db.Close()

	var gift models.Gift
	db.Order(gorm.Expr("rand()")).First(&gift)

	if gift.ID == 0 {
		return
	}

	r.Prize.Type = PrizeTypeGift
	r.Prize.Value = gift
}

func (r *Raffle) randomMoney() {
	r.Prize.Type = PrizeTypeMoney
	r.Prize.Value = minMoney + rand.Float64()*(maxMoney-minMoney)
}

func (r *Raffle) randomPoints() {
	r.Prize.Type = PrizeTypePoints
	r.Prize.Value = rand.Intn(maxPoints-minPoints) + minPoints
}

func (r *Raffle) randomPrize() {
	rand.Seed(time.Now().UnixNano())
	prizeType := getRandomPrizeType()

	switch prizeType {
	case PrizeTypeMoney:
		r.randomMoney()
		return

	case PrizeTypeGift:
		r.randomGift()
		if r.Prize.Value != nil {
			return
		}
	}

	r.randomPoints()
}
