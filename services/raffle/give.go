package raffle

import (
	"fmt"
	"reflect"

	"bitbucket.org/art-es/slotegrator_go/database"
	"bitbucket.org/art-es/slotegrator_go/models"
)

// GivePrize to player.
func (r *Raffle) GivePrize() {
	switch r.Prize.Type {
	case PrizeTypeMoney:
		r.giveMoney()

	case PrizeTypeGift:
		r.giveGift()
	}

	r.givePoints()
}

func (r *Raffle) giveGift() {
	gift := r.Prize.Value
	if gift == nil {
		panic("gift not found")
	}

	db, _ := database.Connect()
	defer db.Close()

	tx := db.Begin()

	var gifts []models.Gift
	db.Model(&r.Player).Related(&gifts, "Gifts")

	fmt.Printf("%#v", gifts)
	giftValue := reflect.ValueOf(gift)
	db.Model(&gift).
		Where(
			"id = ?",
			giftValue.FieldByName("ID").Int(),
		).
		Update(
			"amount",
			giftValue.FieldByName("Amount").Int()-1,
		)

	tx.Commit()
}

func (r *Raffle) giveMoney() {
	db, _ := database.Connect()
	defer db.Close()

	r.Player.Money += reflect.ValueOf(r.Prize.Value).Float()
	db.Save(r.Player)
}

func (r *Raffle) givePoints() {
	db, _ := database.Connect()
	defer db.Close()

	r.Player.Points += int(reflect.ValueOf(r.Prize.Value).Int())
	db.Save(r.Player)
}
