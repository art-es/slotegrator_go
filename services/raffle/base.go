package raffle

import "bitbucket.org/art-es/slotegrator_go/models"

// Raffle data.
type Raffle struct {
	Player *models.Player
	Prize  *Prize
}

// Prize data.
type Prize struct {
	Type  string      `json:"type"`
	Value interface{} `json:"value"`
}

// New instance of raffle.
func New(p *models.Player) *Raffle {
	return &Raffle{
		Player: p,
		Prize:  &Prize{},
	}
}
