package logging

import (
	"log"
	"os"
)

var (
	// FilePath to logging file.
	// Path begin at the "storage/".
	FilePath = "server.log"

	logfile *os.File
	err     error
)

// Close logging data to file.
func Close() {
	_ = logfile.Close()
}

func fullFilePath() string {
	return "storage/" + FilePath
}

// Init initialization logging all output to "server.log" file.
func Init() {
	logfile, err = os.OpenFile(
		fullFilePath(),
		os.O_APPEND|os.O_CREATE|os.O_RDWR,
		0755,
	)
	if err != nil {
		log.Fatal(err)
	}
	log.SetOutput(logfile)
}
