package convert

import (
	"bitbucket.org/art-es/slotegrator_go/database"
	"bitbucket.org/art-es/slotegrator_go/models"
	"math"
)

// MoneyFactor constant value is factor for converting money to points.
const MoneyFactor = 2.5

type Converter struct {
	Player *models.Player
}

// New instance of Converter
func New(player *models.Player) *Converter {
	return &Converter{Player: player}
}

// MoneyToPoints converting user money to points by MoneyFactor
func (c *Converter) MoneyToPoints(pack float64) {
	money := pack
	if c.Player.Money < pack {
		money = c.Player.Money
	}

	c.Player.Points += int(math.Floor(money * MoneyFactor))
	c.Player.Money -= money

	db, _ := database.Connect()
	defer db.Close()

	db.Save(&c.Player)
}
