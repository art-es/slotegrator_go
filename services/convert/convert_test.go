package convert

import (
	"github.com/stretchr/testify/assert"
	"math"
	"testing"
)

type m2p struct {
	beforeMoney  float64
	beforePoints int

	afterMoney  float64
	afterPoints int

	pack           float64
	moneyInStorage int
}

var m2pData = []m2p{
	{100, 0, 0, 250, 100, 100},
}

func TestFloorFunc(t *testing.T) {
	tests := []struct {
		firstNum  float64
		secondNum float64
		result    int
	}{
		{1, 2.5, 2},
		{2.3, 3, 6},
		{1, 1.51, 1},
	}

	for _, test := range tests {
		resultFloat := math.Floor(test.firstNum * test.secondNum)
		assert.Equal(t, test.result, int(resultFloat))
	}
}
