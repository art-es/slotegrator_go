package models

// Player data.
// User whose play raffle.
type Player struct {
	ID     int     `gorm:"primary_key" json:"id"`
	Name   string  `gorm:"unique;not null" json:"name"`
	Money  float64 `gorm:"type:decimal(15,7);default:0" json:"money"`
	Points int     `gorm:"default:0" json:"points"`

	// Relationship
	Gifts []*Gift `gorm:"many2many:player_gift" json:"gifts"`
}
