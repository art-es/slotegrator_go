package models

// Gift data.
// Random things from raffle.
type Gift struct {
	ID     int    `gorm:"primary_key" json:"id"`
	Name   string `gorm:"not null" json:"name"`
	Amount int    `gorm:"default:0" json:"amount"`
}

// PlayerGift pivot data.
type PlayerGift struct {
	PlayerID int `json:"player_id"`
	GiftID   int `json:"gift_id"`
}
