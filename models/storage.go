package models

// Storage data.
// Key-value storage at the database.
type Storage struct {
	Key   string `gorm:"not null" json:"key"`
	Value string `gorm:"text" json:"value"`
}

const (
	// Value of money amounting
	StorageKeyMoneyAmount = "money_amount"
)
