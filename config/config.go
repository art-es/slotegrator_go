package config

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"os"
)

// Specification type of config items.
type Specification struct {
	Port       int
	DBUser     string
	DBPassword string
	DBHost     string
	DBPort     int
	DBName     string
}

var (
	// Config variable where contains configuration data.
	Config = &Specification{
		Port:   8000,
		DBHost: "127.0.0.1",
		DBPort: 3306,
	}

	// FilePath to configuration file.
	// Path begin at the "storage/".
	FilePath = "config.json"
)

func create() {
	file, err := os.Create(fullFilePath())
	if err != nil {
		log.Fatal(err)
	}

	data, err := json.Marshal(&Config)
	if err != nil {
		log.Fatal(err)
	}

	_, err = file.Write(data)
	if err != nil {
		log.Fatal(err)
	}
}

func fullFilePath() string {
	return "storage/" + FilePath
}

// Init initialization configuration file.
// Opens up the file, parsing him & set data to config variable.
// If file doesn't exists, file will be created.
func Init() {
	data, err := ioutil.ReadFile(fullFilePath())
	if err != nil {
		create()
		return
	}

	err = json.Unmarshal(data, &Config)
	if err != nil {
		log.Fatal(err)
	}
}
