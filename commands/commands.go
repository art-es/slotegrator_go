package commands

import "os"

// Command data of command.
type Command struct {
	Signature  string
	HandleFunc CommandHandleFunc
}

// CommandHandleFunc data of handle func.
type CommandHandleFunc func()

// Commands data of command.
// Contains signature & handler.
var Commands = []Command{
	{"send:money", SendMoneyHandle},
}

func RunCommand() (exit bool) {
	args := os.Args

	if len(args) < 2 {
		return
	}

	signature := args[1]
	for _, cmd := range Commands {
		if cmd.Signature == signature {
			cmd.HandleFunc()
			return true
		}
	}

	return
}
