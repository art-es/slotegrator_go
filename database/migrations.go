package database

import (
	"fmt"
	"log"
	"reflect"

	"bitbucket.org/art-es/slotegrator_go/models"
	"github.com/jinzhu/gorm"
)

var (
	tables = []interface{}{
		&models.Player{},
		&models.Gift{},
		&models.Storage{},
		&models.PlayerGift{},
	}

	storageRows = []models.Storage{
		{Key: models.StorageKeyMoneyAmount, Value: "0"},
	}
)

// RunMigrations ...
func RunMigrations() {
	db, err := Connect()
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	run(db)
	fmt.Println("Migrating complete.")
}

func run(db *gorm.DB) {
	createTablesIfNotExists(db)
	checkStorageTable(db)
}

func checkStorageTable(db *gorm.DB) {
	for _, row := range storageRows {
		fmt.Printf("Check row with key %s in storage table\n", row.Key)
		var count int
		db.Model(&row).Where("key = ?", row.Key).Count(&count)

		if count > 0 {
			continue
		}

		fmt.Printf("Add row with key %s to storage table\n", row.Key)
		db.Create(&row)
	}
}

func createTablesIfNotExists(db *gorm.DB) {
	for _, table := range tables {
		label := tableLabel(table)
		fmt.Printf("check database for existing: %v\n", label)
		if !db.HasTable(table) {
			fmt.Printf("create table: %s\n", label)
			db.CreateTable(table)
		}
	}
}

func tableLabel(table interface{}) interface{} {
	typeof := reflect.TypeOf(table).String()
	if typeof == "string" {
		return table
	}
	return typeof
}
