package database

import (
	"fmt"

	"bitbucket.org/art-es/slotegrator_go/config"
	"github.com/jinzhu/gorm"

	// mysql driver
	_ "github.com/jinzhu/gorm/dialects/mysql"
)

// Connect to database.
func Connect() (*gorm.DB, error) {
	cnf := config.Config

	conn := fmt.Sprintf(
		"%s:%s@(%s:%d)/%s?charset=utf8&parseTime=True&loc=Local",
		cnf.DBUser, cnf.DBPassword, cnf.DBHost, cnf.DBPort, cnf.DBName,
	)

	db, err := gorm.Open("mysql", conn)
	if err != nil {
		return nil, err
	}

	db.SingularTable(true)
	return db, nil
}
