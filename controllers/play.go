package controllers

import (
	"net/http"

	"bitbucket.org/art-es/slotegrator_go/database"
	"bitbucket.org/art-es/slotegrator_go/models"
	"bitbucket.org/art-es/slotegrator_go/services/raffle"
	"github.com/gin-gonic/gin"
)

// Play raffle
// Route: GET "/play"
func Play(ctx *gin.Context) {
	db, err := database.Connect()
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, gin.H{
			"success": false,
			"message": "database cannot connected",
			"error":   err.Error(),
		})
		return
	}

	var player models.Player
	db.Model(&player).First(&player)

	r := raffle.New(&player)
	r.Play()
	r.GivePrize()

	ctx.JSON(http.StatusOK, gin.H{
		"success": true,
		"player":  player,
		"type":    r.Prize.Type,
		"value":   r.Prize.Value,
	})
}
