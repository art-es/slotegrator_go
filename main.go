package main

import (
	"bitbucket.org/art-es/slotegrator_go/commands"
	"flag"
	"fmt"
	"log"
	"net/http"

	"bitbucket.org/art-es/slotegrator_go/config"
	"bitbucket.org/art-es/slotegrator_go/controllers"
	"bitbucket.org/art-es/slotegrator_go/database"
	"bitbucket.org/art-es/slotegrator_go/services/logging"
	"github.com/gin-gonic/gin"
)

var isMigrate = flag.Bool("migrate", false, "run migrations")

func main() {
	if exit := flags(); exit {
		return
	}

	if exit := commands.RunCommand(); exit {
		return
	}

	router := gin.New()
	router.GET("/play", controllers.Play)

	srv := http.Server{
		Addr:    fmt.Sprintf(":%d", config.Config.Port),
		Handler: router,
	}
	log.Fatal(srv.ListenAndServe())
}

func init() {
	flag.Parse()
	logging.Init()
	config.Init()

	// Check database connection. (As "ping-pong" in NoSQL.)
	db, err := database.Connect()
	if err != nil {
		log.Fatal(err)
	}
	db.Close()
}

func flags() (exit bool) {
	if *isMigrate {
		database.RunMigrations()
		return true
	}

	return
}
